module exmaple.com/helloapp

go 1.16

replace exmaple.com/helloapp/eng => ./eng

replace exmaple.com/helloapp/th => ./th

require (
	exmaple.com/helloapp/eng v0.0.0-00010101000000-000000000000
	exmaple.com/helloapp/th v0.0.0-00010101000000-000000000000
)
