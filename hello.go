package hello

import "exmaple.com/helloapp/eng"
import "exmaple.com/helloapp/th"

func Hello() string {
	return eng.Greet()
}

func HelloTh() string {
	return th.Greet()
}
