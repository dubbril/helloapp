package eng

import "gitlab.com/dubbril/quote"

func Greet() string {
	return quote.Speak()
}
