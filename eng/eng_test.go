package eng

import "testing"

func TestGreet(t *testing.T) {
	want := "Hi, mate"
	if got := Greet(); got != want {
		t.Errorf("Greet() = %q, want : %q", got, want)
	}
}
