package hello

import "testing"

func TestHello(t *testing.T) {
	want := "Hi, mate"
	if got := Hello(); got != want {
		t.Errorf("Hello() = %q, want : %q", got, want)
	}
}

func TestThHello(t *testing.T) {
	want := "สวัสดี-Hi, mate"
	if got := HelloTh(); got != want {
		t.Errorf("HelloTh() = %q, want : %q", got, want)
	}
}
